import crypto from 'crypto';

//TODO use something else! and put it inside a .env file
const SECRET = 'SUperSecretZit'

// TODO: maybe explain what is this (it's just hashing and salting)  and why we did 128 and base64?

// Generage a random string of 128 characters and store it in base64
export const random = () => crypto.randomBytes(128).toString('base64');

export const authentification = (salt, password) => {
    return crypto.createHmac('sha256', [salt, password].join('/')).update(SECRET).digest('hex');
};
