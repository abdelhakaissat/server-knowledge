import multer from 'multer';
import { getFileNameFromCourse } from '../db/courses.js';

// Custom file filter function
const fileFilter = async (req, file, cb) => {
    // Check file type, size, or any other criteria
    const result = await getFileNameFromCourse(req.body.courseName, file.originalname);

    if (result.length > 0) {
        const error = new Error('File Already Exists');
        error.code = 'FILE_FILTER_ERROR';
        error.stack = null;
        error.message = 'File Already Exists';
        cb(error, false); // Reject the file
    } else {
        //file doesn't exist so we aceppt it!
        cb(null, true); // Accept the file

    }
};

const upload = multer({ dest: 'src/uploads/', fileFilter: fileFilter })
export { upload };
