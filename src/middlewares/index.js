import  express from "express";
import lodash from "lodash";
const {get, merge} = lodash;

import { getUserBySessionToken } from "../db/users.js";

export const isAuthenticated = async (req, res, next) => {
    try{
        const sessionToken = req.cookies.sessionToken;

        //user has no session token
        if (!sessionToken) {
            return res.status(401).json({ error: "Unauthorized" });
        }

        const existingUser = await getUserBySessionToken(sessionToken);
        if (!existingUser) {
            return res.status(401).json({ error: "Unauthorized" });
        }

        // if the user exists  we add it's identity to the request
        merge(req, { identity: existingUser});
        return next();

    }catch(error){
        console.log(error);
        res.status(500).json({ error: error.message });
    }

};