import mongoose from 'mongoose';

// we can use strict: false
const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 32,
        unique: true,
        //custom, might be useful for middleware
        trim: true
    },
    email: {
        type: String,
        required: true,
        //custom  might be useful for middleware
        trim: true,
    },
    authentification: {
        password: {
            type: String,
            required: true,
            select: false,// to not return password in response
        },
        salt: {
            type: String,
            select: false,
        },
        sessionToken: {
            type: String,
            select: false,
        },
    }
});

//turn this schema into a model
export const UserModel = mongoose.model('User', userSchema);



// TODO: double check this !
// This if for the user controller
export const getUsers = async () => UserModel.find();
export const getUserByEmail = async (email) => {
    return UserModel.findOne({ email: email });
};
//  findOne() instead of find() because it returns only one user
// TODO: What if there is more then one in the db?? what would that mean?
export const getUserBySessionToken = async (sessionToken) => UserModel.findOne({ 'authentification.sessionToken': sessionToken });
export const getUserById = async (id) => UserModel.find({ _id: id });
export const getUserByUsername = async (username) => UserModel.find({ username: username });




export const createUser = async (values) => {
    //TODO: Im not sure about this ! recheck it
    new UserModel(values).save().then((user) => user.toObject());
};

export const deleteUser = async (id) => { UserModel.findOneAndDelete({ _id: id }) };

export const updateUpserById = async (id, values) => { UserModel.findByIdAndUpdate(id, values) };








/*
// create a user in the db, not sure about the syntax though \\

export const createUser = (username, password, email) => {
    const user = {
        username: username,
        password: password,
        email: email
    };
    //TODO: Im not sure about this ! recheck it
    new UserModel(user).save().then((user) => {user.toObject()});
};

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 32,
        unique: true,
        trim: true,
    },
    password: {
        type: String,
        required: true,
        minlength: 8,
        maxlength: 32,
        trim: true,
        select: false,
    },
    email: {
        type: String,
        required: true,
        minlength: 8,
        maxlength: 32,
        trim: true,
    },
    role: {
        type: String,
        required: true,
        enum: ['admin', 'user'],
        default: 'user',
    },
    status: {
        type: String,
        required: true,
        enum: ['active', 'inactive'],
        default: 'active',
    },
    created_at: {
        type: Date,
        default: Date.now,
    },
    updated_at: {
        type: Date,
    },
});

*/