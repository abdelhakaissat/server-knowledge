import mongoose from 'mongoose';

const Schema = mongoose.Schema;
// Level Schema
const LevelSchema = new Schema({
    levelName: {
        type: String,
        unique: true
    },
    yearID: {
        type: Schema.Types.ObjectId,
        ref: 'Year'
    },
    levelNumber: {
        type: Number,
    }
});

export const levelModel = mongoose.model('Level', LevelSchema);


// Level 
export const getLevel = async (level) => levelModel.find({ levelName: level });//tested
export const getLevelById = async (id) => levelModel.findById(id);
export const getLevelByNumber = async (number) => levelModel.find({ levelNumber: number });
export const getAlllevels = async () => levelModel.find({});//tested
export const createLevel = async (level) => levelModel.create(level);//tested

