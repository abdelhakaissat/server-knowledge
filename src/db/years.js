import mongoose from 'mongoose';

const Schema = mongoose.Schema;
// Year Schema
const YearSchema = new Schema({
    yearName: {
        type: Number,
        unique: true
    }
});
export const yearModel = mongoose.model('Year', YearSchema);

// Year
export const getYears = async (year) => yearModel.find({ year: year });//(tested)
export const createYear = async (year) => yearModel.create(year);//tested
