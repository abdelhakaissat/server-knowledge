import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// Course Schema
const CourseSchema = new Schema({
    courseName: {
        type: String,
        unique: true
    },
    levelID: {
        type: Schema.Types.ObjectId,
        ref: 'Level'
    },
    files: [],
});

export const courseModel = mongoose.model('Course', CourseSchema);



// Course
export const getCourseByName = (course) => courseModel.find({ courseName: course });
export const getAllCourses = async () => courseModel.find({});//(tested)
export const getAllCoursesNamesOnly = async () => courseModel.find({}, { courseName: 1, _id: 0 });//tested
export const createCourse = async (course) => courseModel.create(course);

//TODO test these functions
export const addFileToCourse = async (course, file) => courseModel.updateOne({ courseName: course }, { $push: { files: file } });
export const getFilesFromCourse = async (course) => courseModel.find({ courseName: course }, { files: 1, _id: 0 });//return files don't return the ids
export const getFileNameFromCourse = async (course, filename) => courseModel.find({
    courseName: course,
    files: { $elemMatch: { originalname: filename } }
});



export const getAllFiles = async () => courseModel.find({}, { files: 1, _id: 0 });//return files don't return the ids

export const gettAllCoursesLevel = async (level) => courseModel.find({ levelID: level });


//push adds a an elemnt to an array but set adds a value
export const addLevelToCourse = async (course, level) => courseModel.updateOne({ courseName: course }, { $set: { levelID: level } });//tested