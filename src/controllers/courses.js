import { getAllCourses, getAllCoursesNamesOnly, getAllFiles, addFileToCourse } from '../db/courses.js'

export const getCoursesNames = async (req, res) => {
    try {
        const allCourses = await getAllCoursesNamesOnly();
        res.status(200).json(allCourses).end();
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

export const getCourses = async (req, res) => {
    try {
        const allCourses = await getAllCourses();
        res.status(200).json(allCourses).end();
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

//TODO: TEST THIS
export const getFiles = async (req, res) => {
    try {
        const allFiles = await getAllFiles();
        res.status(200).json(allFiles).end();
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};


export const addFile = async (req, res) => {
    try {
        const { course, file } = req.body;
        // check if the cours exists
        const something = await addFileToCourse(course, file);
        res.status(200).json(something).end();
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: error.message });
    }
};

export const addFiles = async (req, res) => {
    try {
        const { course, level, files } = req.body;
        // check if the cours exists
        const allCourses = await getAllCourses();
        res.status(200).json(allCourses).end();
    } catch (error) {
        res.status(500).json({ error: error.message });
    }

};