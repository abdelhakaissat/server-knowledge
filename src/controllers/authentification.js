import { getUserByEmail, getUsers, createUser } from '../db/users.js';
import { random, authentification } from '../helpers/index.js';

export const register = async (req, res) => {

    try {
        const { username, password, email } = req.body;
        console.log(req.body);
        if (!username || !password || !email) {
            return res.status(400).json({ error: 'Missing fields' });
        }

        const existingUser = await getUserByEmail(email);
        console.log("just after existingUser: ", existingUser)
        const allusers = await getUsers();
        console.log("all users: ", allusers);


        if (existingUser) {
            return res.status(400).json({ error: 'Sorry there was an error you should do something about it' });
        }

        const salt = random();
        console.log("user doens't exist");
        const user = await createUser({
            username,
            email,
            authentification: {
                salt,
                password: authentification(salt, password),
            }
        });

        // everything is alright and working
        return res.status(201).json(user).end();

    } catch (error) {
        console.log(error);
        res.status(500).json({ error: error.message });
    }
}

export const login = async (req, res) => {
    console.log("inside login");
    try {
        const { email, password } = req.body;
        if (!email || !password) {
            return res.status(400).json({ error: 'Missing fields' });
        }

        // the select and plus are used to get the salt from the db
        const user = await getUserByEmail(email).select('+authentification.salt +authentification.password');
        console.log("user: ", user);
        console.log("usename: ", user.username);
        console.log("salt: ", user.authentification.salt);
        // user doens't exist
        if (!user) {
            return res.status(400).json({ error: 'Sorry there was an error everything is wrong with you and you should do something about it' });
        }
        const expectedHash = authentification(user.authentification.salt, password);

        if (user.authentification.password !== expectedHash) {
            return res.status(400).json({ error: 'Sorry there was an error everything is wrong with you and you should do something about it' });
        }

        // allow user to login
        const salt = random();
        // TODO: check if this is the right way to generate sessionTokens (probably not ?)
        // TODO: use tools such as expressValidator 
        // TODO: jwt  maybe?
        // TODO: do more checking for email correctness ...
        user.authentification.sessionToken = authentification(salt, user._id.toString());

        //we save the new data in the db
        await user.save();
        //TODO: double check the parameters of the cookie
        res.cookie('sessionToken', user.authentification.sessionToken, { domain: 'localhost', path: '/', httpOnly: true, secure: false, sameSite: true });

        return res.status(200).json(user).end();


    } catch (error) {
        console.log(error);
        res.sendStatus(400);
    }

}
