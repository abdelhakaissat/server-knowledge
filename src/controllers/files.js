import { addFileToCourse, getAllFiles } from "../db/courses.js";
import fs from 'fs';
import path from "path";

export const saveFiles = async (req, res) => {
    try {
        //TODO save the file path into the database
        //TODO: check if the file already exists on the database
        //TODO: Get the course name from the request
        const courseName = req.body.courseName;
        const file = req.file;
        const newFile = await addFileToCourse(courseName, file)
        res.status(200).json({ "files": "have been uploaded?" }).end();

    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

export const serveFile = async (req, res) => {
    try {
        const path_dir = "src/uploads/"

        const fileId = req.params.id;
        const filePath = path.join(path_dir, fileId);

        const stat = fs.statSync(filePath);
        res.writeHead(200, {
            'Content-Type': 'application/pdf',
            'Content-Length': stat.size,
        });

        const readStream = fs.createReadStream(filePath);
        readStream.pipe(res);

    } catch (error) {
        console.log(error);
        res.status(500).json({ error: error.message });
    }
};

export const serveFiles = async (req, res) => {
    try {
        const allFiles = await getAllFiles();
        const realFilesPaths = [];

        //TODO: do this in a better way (just do a good query)
        allFiles.forEach(course => {
            course = course.files;
            if (course.length > 0) {
                course.forEach(
                    file => {
                        realFilesPaths.push(file);
                    }
                )
            }

        });
        res.status(200).json(realFilesPaths).end();
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: error.message });
    }
}