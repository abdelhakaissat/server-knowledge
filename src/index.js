import express from 'express';
import http  from 'http';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import compression from 'compression';
import cors from 'cors';
import mongoose from 'mongoose';
import {router} from './router/index.js';

import url from 'url';
import path from 'path';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express()
const PORT = 8080;

app.use(cors({
    credentials: true,
}));

app.use(compression());
app.use(cookieParser());
app.use(bodyParser.json())

const server = http.createServer(app);

server.listen(PORT, () => {
    console.log(`Server is listening on port http://localhost:${PORT}`);
});




const MONGO_URL = "mongodb://127.0.0.1:27017/knowledge_db"

mongoose.connect(MONGO_URL);

mongoose.connection.on('connected', () => {
    console.log('Connected to mongo instance');
});
mongoose.connection.on('error', () => {
    console.log(error);
});


//manage routes (it's in the end of the file in )
app.use(router);

//in rder to serve files from the uploads directory
app.use(express.static(path.join(__dirname, 'uploads')));