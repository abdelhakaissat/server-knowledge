import express from'express';
import {register, login} from '../controllers/authentification.js';
import { getAllUsers } from '../controllers/users.js';
import { isAuthenticated } from '../middlewares/index.js';
import {addFile, getCoursesNames, getCourses} from "../controllers/courses.js";
import {upload} from "../middlewares/saveMulter.js";
import { saveFiles, serveFile, serveFiles } from '../controllers/files.js';
import url from 'url';
import path from 'path';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const router = express.Router();

router.post('/auth/register', register);
router.post('/auth/login', login);


//TODO maybe have a different file for each "ressource"
router.get("/users",isAuthenticated, getAllUsers)
//router.get("/tests",testFunc)

router.get("/courses/names", getCoursesNames);
router.get("/courses", getCourses);

router.get("/files/upload",  upload.single('file'), saveFiles )

// Serve files from the 'uploads' directory (useful with multer)
router.get('/files/', serveFiles);
router.get('/files/:id', express.static(path.join(__dirname, 'uploads')), serveFile);

/*TODO: test and implement all of these! */
//router.post("/courses/files", addFile);
//router.get("/courses/:CourseName/files", getCourses);
//router.get("/courses/:id", getCourses);
//router.delete("/courses")


/* difference betwwen this and the other export:  
    - this is called the **default export** it allows you to export a single entity from a module 
        when you import from a module that has a default export you don't need to use the curly
        braces or know the name of the entity you are importing

    - in the other hand the other way of doing it (export const something = "yey") is called the 
        **named export** it allows you to export multiple entities from a module
        when you import from a module that has a named export you need to know the exact name of 
        the exported entity 
*/

//export default router;
export {router};